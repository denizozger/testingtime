import { Question } from "./models/Question";

export const smartphoneQuestion = new Question(
  "1",
  "Which smartphone do you own?",
  ["Android", "iPhone", "Windows", "Other"]
);

export const travelQuestion = new Question(
  "2",
  "How do you travel most regularly?",
  ["By car", "By public transport", "By bicycle", "Other"]
);
