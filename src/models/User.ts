interface IUser {
  id: string;
  email: string;
}

class User implements IUser {
  id: string;
  email: string;

  constructor(id: string, email: string) {
    this.id = id;
    this.email = email;
  }
}

export { User, IUser };
