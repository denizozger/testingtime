interface IQuestion {
  id: string;
  question: string;
  answers: string[];
}

class Question implements IQuestion {
  id: string;
  question: string;
  answers: string[];

  constructor(id: string, question: string, answers: string[]) {
    this.id = id;
    this.question = question;
    this.answers = answers;
  }

  getAnswerIndex(answer: string) {
    return this.answers.indexOf(answer);
  }
}

export { Question, IQuestion };
