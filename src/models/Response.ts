interface IResponse {
  userId: string;
  questionId: string;
  answeredIndex: number;
}

class Response implements IResponse {
  userId: string;
  questionId: string;
  answeredIndex: number;

  constructor(userId: string, questionId: string, answeredIndex: number) {
    this.userId = userId;
    this.questionId = questionId;
    this.answeredIndex = answeredIndex;
  }
}

export { Response, IResponse };
