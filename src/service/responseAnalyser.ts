import { IResponse } from "../models/Response";
import { smartphoneQuestion, travelQuestion } from "../constants";

function isRespondedByAnIPhoneUser(response: IResponse) {
  return (
    response.questionId === smartphoneQuestion.id &&
    response.answeredIndex === smartphoneQuestion.answers.indexOf("iPhone")
  );
}

function isRespondedByACarCommuter(response: IResponse) {
  return (
    response.questionId === travelQuestion.id &&
    response.answeredIndex == travelQuestion.answers.indexOf("By car")
  );
}

interface UserIdAnswerMap {
    [userId: string]: {
        isAnIPhoneUser: boolean;
        travelsByCar: boolean;
    };
}

function countNumberOfIPhoneUsersWhoTravelByCar(responses: IResponse[]) {
  const userIdAnswerMap = responses.reduce(
    (userIdAnswerMap: UserIdAnswerMap, response) => {
      const userId = response.userId;

      if (!userIdAnswerMap[userId]) {
        userIdAnswerMap[userId] = {
          isAnIPhoneUser: false,
          travelsByCar: false,
        };
      }

      if (isRespondedByAnIPhoneUser(response)) {
        userIdAnswerMap[userId].isAnIPhoneUser = true;
      }

      if (isRespondedByACarCommuter(response)) {
        userIdAnswerMap[userId].travelsByCar = true;
      }

      return userIdAnswerMap;
    },
    {}
  );

  return Object.entries(userIdAnswerMap).reduce(
    (totalCountOfUsers, [, { isAnIPhoneUser, travelsByCar }]) => {
      if (isAnIPhoneUser && travelsByCar) {
        return totalCountOfUsers + 1;
      }
      return totalCountOfUsers;
    },
    0
  );
}

export { countNumberOfIPhoneUsersWhoTravelByCar };
