import { User } from "../src/models/User";
import { Response } from "../src/models/Response";
import { countNumberOfIPhoneUsersWhoTravelByCar } from "../src/service/responseAnalyser";
import { smartphoneQuestion, travelQuestion } from "../src/constants";

const userOne = new User("1", "foo@bar.com");
const userTwo = new User("2", "foo@baz.com");

describe("Testing Time", () => {
  const responses = [
    new Response(
      userOne.id,
      smartphoneQuestion.id,
      smartphoneQuestion.getAnswerIndex("iPhone")
    ),
    new Response(
      userOne.id,
      travelQuestion.id,
      travelQuestion.getAnswerIndex("By car")
    ),
    new Response(
      userTwo.id,
      smartphoneQuestion.id,
      smartphoneQuestion.getAnswerIndex("Android")
    ),
    new Response(
      userTwo.id,
      travelQuestion.id,
      travelQuestion.getAnswerIndex("By bicycle")
    ),
  ];

  it("counts number of users who own an iPhone and travel by car", () => {
    const expectedCount = 1;
    const actualCount = countNumberOfIPhoneUsersWhoTravelByCar(responses);

    expect(actualCount).toEqual(expectedCount);
  });
});
